import java.util.Scanner;

public class CodingJava24 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Input Number Decimal: ");
        int numDec = scanner.nextInt();

        if (numDec > 0) {
            int num = numDec;
            StringBuilder numBinary = new StringBuilder();
            while (num > 0) {
                int x = num % 2;
                numBinary.append(String.valueOf(x));
                num /= 2;
            }
            StringBuilder result = new StringBuilder();
            for (int i = numBinary.length() - 1; i > 0; i--) {
                result.append(numBinary.charAt(i));
            }
            System.out.println("Number Binary for "+ numDec +" : "+result);
        } else {
            System.out.println("You should be add number more than 0");
        }

    }
}
